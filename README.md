# In Cluster Client Configuration


go mod init in-cluster-client-configuration

docker buildx build -t "remotejob/in-cluster-client-configuration:0.0" --platform linux/amd64,linux/arm64 --push .

kubectl create clusterrolebinding default-view --clusterrole=view --serviceaccount=default:default

kubectl run --rm -i demo --image=remotejob/in-cluster-client-configuration:0.0

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

minikube image load in-cluster

minikube image ls --format table

docker build -t first-image -f ./Dockerfile .

kubectl run first-container --image=first-image --image-pull-policy=Never --restart=Never