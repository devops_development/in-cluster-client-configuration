/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Note: the example only works with the code within the same release/branch.
package main

import (
	"context"
	"fmt"
	"log"

	batchv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"

	// "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
)

var clientset *kubernetes.Clientset

const jobName = "test-job-clientgo"

func init() {

	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

}

func main() {
	createJob() 
	// creates the in-cluster config

	// for {
	// 	// get pods in all the namespaces by omitting namespace
	// 	// Or specify namespace to get pods in particular namespace
	// 	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
	// 	if err != nil {
	// 		panic(err.Error())
	// 	}
	// 	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

	// 	// Examples for error handling:
	// 	// - Use helper functions e.g. errors.IsNotFound()
	// 	// - And/or cast to StatusError and use its properties like e.g. ErrStatus.Message
	// 	// _, err = clientset.CoreV1().Pods("default").Get(context.TODO(), "example-xxxxx", metav1.GetOptions{})
	// 	// if errors.IsNotFound(err) {
	// 	// 	fmt.Printf("Pod example-xxxxx not found in default namespace\n")
	// 	// } else if statusError, isStatus := err.(*errors.StatusError); isStatus {
	// 	// 	fmt.Printf("Error getting pod %v\n", statusError.ErrStatus.Message)
	// 	// } else if err != nil {
	// 	// 	panic(err.Error())
	// 	// } else {
	// 	// 	fmt.Printf("Found example-xxxxx pod in default namespace\n")
	// 	// }

	// 	time.Sleep(10 * time.Second)
	// }
}

func createJob() {

	fmt.Println("creating job", jobName)

	jobsClient := clientset.BatchV1().Jobs(apiv1.NamespaceDefault)

	job := &batchv1.Job{ObjectMeta: metav1.ObjectMeta{

		Name: jobName,
	},

		Spec: batchv1.JobSpec{

			Template: apiv1.PodTemplateSpec{

				Spec: apiv1.PodSpec{

					Containers: []apiv1.Container{

						{

							Name: "pi",

							Image: "perl:5.34.0",

							Command: []string{"perl", "-Mbignum=bpi", "-wle", "print bpi(2000)"},
						},
					},

					RestartPolicy: apiv1.RestartPolicyNever,
				},
			},
		},
	}

	_, err := jobsClient.Create(context.Background(), job, metav1.CreateOptions{})

	if err != nil {

		log.Fatal("failed to create job", err)

	}

	fmt.Println("created job successfully")

}
